<?php 
session_start();


// Intializing the application - DO NOT TOUCH IF YOU DON'T KNOW WHAT YOU ARE DOING...
require_once dirname(__FILE__).'/../vendor/autoload.php';
require_once dirname(__FILE__).'/php_fast_cache.php';
require_once dirname(__FILE__).'/functions.php';
use Propel\Runtime\Propel;
use Propel\Runtime\Connection\ConnectionManagerSingle;
use DebugBar\StandardDebugBar;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Logger
$app['logger'] = new Logger('defaultLogger');
$app['logger']->pushHandler(new StreamHandler(dirname(__FILE__).'/../logs/error-'.date('d-m-Y', time()).'.log', Logger::ERROR));
$app['logger']->pushHandler(new StreamHandler(dirname(__FILE__).'/../logs/critical-'.date('d-m-Y', time()).'.log', Logger::CRITICAL));

// Cache
$app['cache'] = new phpFastCache();
$app['cache']::$path = dirname(__FILE__).'/..';

// Propel ORM
$serviceContainer = Propel::getServiceContainer();
$serviceContainer->setAdapterClass('discinfo', 'mysql');
$serviceContainer->setLogger('defaultLogger', $app['logger']);
$app['connection'] = new ConnectionManagerSingle();
$app['connection']->setConfiguration(array(
  'dsn'      => 'mysql:host='.$app['config']['db']['primary']['server'].';dbname='.$app['config']['db']['primary']['database'],
  'user'     => $app['config']['db']['primary']['username'],
  'password' => $app['config']['db']['primary']['password'],
));
$serviceContainer->setConnectionManager('discinfo', $app['connection']);
$app['db'] = Propel::getWriteConnection('discinfo');

// Prepare Twig template engine
Twig_Autoloader::register();
/*$app['template'] = new Twig_Environment(
	new Twig_Loader_Filesystem(__DIR__.'/../views'), 
	array(
    	'cache' => ($app['config']['debug'] == true) ? false : __DIR__.'/../cache/views',
		'debug' => ($app['config']['debug'] == true) ? true : false,
	)
);*/
$app['template'] = new DebugBar\Bridge\Twig\TraceableTwigEnvironment(
	new Twig_Environment(
		new Twig_Loader_Filesystem(__DIR__.'/../views'), 
		array(
	    	'cache' => (($app['config']['debug'] == true) ? false : __DIR__.'/../cache/views'),
    		'debug' => (($app['config']['debug'] == true) ? true : false),
		)
	)
);


// Preparing debug enviorment
if($app['config']['debug'] == true){
	// Setup Monolog handlers
	$app['logger']->pushHandler(new StreamHandler(dirname(__FILE__).'/../logs/debug-'.date('d-m-Y', time()).'.log', Logger::DEBUG));
	$app['logger']->pushHandler(new StreamHandler(dirname(__FILE__).'/../logs/info-'.date('d-m-Y', time()).'.log', Logger::INFO));

	// Turn ON Propel Logging
	$app['db']->useDebug(true);
	// Set monolog as logger
	$app['db']->setLogger($app['logger']);

	// Initialize Debugbar
	$app['debugger'] = new StandardDebugBar();
	// Monolog
	$app['debugger']->addCollector(new DebugBar\Bridge\MonologCollector($app['logger']));
	// Configs
	$app['debugger']->addCollector(new DebugBar\DataCollector\ConfigCollector($app['config']));
	// Twig
	$app['debugger']->addCollector(new DebugBar\Bridge\Twig\TwigCollector($app['template']));
	// Preparing assets
	$app['debuggerAssets'] = $app['debugger']->getJavascriptRenderer();
	$app['debuggerAssets']->setBaseUrl(app_url('assets'));
}

if($app['config']['debug'] == true || isset($_GET['_cleancache'])){
	$app['cache']->cleanup(); // Cleaning cache
}	

// Registering twig extensions
require_once __DIR__.'/twig.php';

if((! is_ajax_request()) && ($app['config']['error_reporting'] == false)){
	error_reporting(0); // Hide all warnings
}