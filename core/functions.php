<?php

$app['config'] = include dirname(__FILE__).'/../libs/config.php';

if(! function_exists('dd')){
	function dd($data){
		exit(print_r($data));
	}
}

if(! function_exists('fix_date')){
	function fix_date($date, $new_format = 'Y-m-d', $old_format = 'd-m-Y'){
		try {
			return DateTime::createFromFormat('!'.$old_format, trim(str_replace('/', '-', $date)))->format($new_format);
		}
		catch(Exception $e){
			$app['logger']->addError($e->getMessage());
		}
	}
}

if(! function_exists('get_pagination_links')){
	function get_pagination_links($obj, $url, $offset = 5){
		global $app;
		$html = '';
		if($obj->haveToPaginate()){
			$app['logger']->addDebug('Page: '.$obj->getFirstIndex(). ' '.$obj->getLastIndex());
//			$html .= '<a href="'.app_url($url.'?'.query_params(false, array('page' => $obj->getFirstIndex()))).'">First</a>&nbsp;&nbsp;';
			$offsetp = ($obj->getFirstIndex() < (get_value('page', 1)-$offset)) ? $obj->getFirstIndex() : get_value('page', 1)-$offset;
			$app['logger']->addDebug('Previous page '. $offsetp);
			$offsetp = ($offsetp < 1) ? 1 : $offsetp;
			for($i = $offsetp; $i < get_value('page', 1); $i++){

				if(get_value('page') == $i){
					$html .= $i.'&nbsp;&nbsp;';
				}
				else {
					$html .= '<a href="'.app_url($url.'?'.query_params(false, array('page' => $i))).'">'.$i.'</a>&nbsp;&nbsp;';
				}
			}
			$offset = ($obj->getLastIndex() < (get_value('page') + $offset)) ? $obj->getLastIndex() : get_value('page', 1) + $offset;
			for($i = get_value('page', 1); $i <= $offset; $i++){

				if(get_value('page') == $i){
					$html .= $i.'&nbsp;&nbsp;';
				}
				else {
					$html .= '<a href="'.app_url($url.'?'.query_params(false, array('page' => $i))).'">'.$i.'</a>&nbsp;&nbsp;';
				}
			}

//			$html .= '<a href="'.app_url($url.'?'.query_params(false, array('page' => $obj->getLastIndex()))).'">Last</a>';
		}
		return $html;
	}
}

if(! function_exists(('app_url'))){
	function app_url($uri = null){
		global $app;
		return $app['config']['app_url'].'/'.$uri;
	}
}

if(! function_exists('query_params')){
	function query_params($array = false, $otherVars = array()){
		$vars = $_GET;

		// Override
		foreach ($otherVars as $key => $value) {
			$vars[$key] = $value;
		}

		if($array)
			return $vars;

		$parts = array();
		foreach($vars as $key=>$value){
			if(! is_array($value))
				$parts[] = $key.'='.$value;
			else
				foreach($value as $val){
					$parts[] = $key.'[]='.$val;
				}
		}

		return implode('&', $parts);
	}
}

if(! function_exists('redirect')){
	function redirect($uri){
		global $app;
		if(strpos($uri, 'back') !== false){
			$url = str_replace('back', $_SERVER['HTTP_REFERER'], $uri);
		}
		else {
			$url = app_url($uri);
		}

		if($app['config']['debug'] == true)
			$app['debugger']->stackData();

//		header("Location: ".$url);
		echo("<script> window.location= \"".$url."\";</script>");
	}
}

if(! function_exists('get_value')){
	function get_value($key, $default = null){
		if(isset($_GET[$key])) return $_GET[$key];
		else if(isset($_POST[$key])) return $_POST[$key];
		else return $default;
	}
}

/*
objects_total($records, 'getArr');	
*/
if(! function_exists('objects_total')){
	function objects_total($objectArray, $key){
		$total = 0;
		foreach($objectArray as $obj){
			$total += $obj->$key();
		}

		return $total;
	}
}

if(! function_exists('iif')){
	function iif($condition, $true_statement, $false_statement = null){
		if($condition){
			return $true_statement;
		}
		else {
			return $false_statement;
		}
	}
}

if(! function_exists('is_ajax_request')){
	function is_ajax_request(){
		global $app;
		$headers = apache_request_headers();
		$is_ajax = (isset($headers['X-Requested-With']) && $headers['X-Requested-With'] == 'XMLHttpRequest');

		if($is_ajax && $app['config']['debug'] == true){
			$app['debugger']->sendDataInHeaders();
		}

		return $is_ajax;
	}
}


?>