<?php

// Globals
$app['template']->addGlobal('app_url', app_url());

// Filters
$path = new Twig_SimpleFilter('*_url', function ($name, $arguments) use($app) {
	$app['logger']->addDebug($name . $arguments);
	if($name == 'app')
		return app_url($arguments);
});
$app['template']->addFilter($path);

// Functions
$debugbar_head = new Twig_SimpleFunction('debugbar_head', function() use($app) {
	return ($app['config']['debug']) ? $app['debuggerAssets']->renderHead() : '';
}, array('is_safe' => array('html')));
$app['template']->addFunction($debugbar_head);

$debugbar_footer = new Twig_SimpleFunction('debugbar_footer', function() use($app) {
	return ($app['config']['debug']) ? $app['debuggerAssets']->render() : '';
}, array('is_safe' => array('html')));
$app['template']->addFunction($debugbar_footer);

$fields = new Twig_SimpleFunction('*', function($name) use($app) {
	$args = func_get_args();
	unset($args[0]);
	return call_user_func_array($name, $args);
}, array('is_safe' => array('html')));
$app['template']->addFunction($fields);