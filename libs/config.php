<?php

return array(
	'db' => array(
		// Master Database Information
		'primary' => array(
			'server' => '127.0.0.1',
			'username' => 'root',
			'password' => 'daxdaxdax',
			'database' => 'discinfo',
		),
	),

	// Debug Mode will help logging application. 
	// Turning it off is highly recommended on production.
	// However, error/failure in the system will still be logged for troubleshooting.
	'debug' => true,

	'error_reporting' => true,

	// No Trailing slash..please!
	'app_url' => 'http://localhost',

	// Cache Time in seconds
	'cache_time' => 60*60*3, // Seconds*Monutes*Hours = Total Cache Time,
);
